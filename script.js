const apiUrl = "https://ajax.test-danit.com/api/swapi/films";
const main = document.getElementById("main");

async function getData(url) {
  let res = await fetch(url);
  let data = await res.json();
  return data;
}

function showMovies(url) {
  main.innerHTML = "";
  let counter = 0;
  let totalCounter = 0;
  getData(url).then((movies) =>
    movies.forEach((movie) => {
      const { episodeId, name, openingCrawl, characters } = movie;
      const chrEl = document.createElement("ul");
      chrEl.classList.add("characters");
      console.log(characters.length);
      totalCounter += characters.length;
      console.log(totalCounter);
      characters.forEach((characterUrl) => {
        getData(characterUrl).then((character) => {
          counter += 1;
          let li = document.createElement("li");
          li.innerText = character.name;
          chrEl.appendChild(li);
          if (totalCounter == counter) {
            hideloader();
          }
        });
      });    

      const movieEl = document.createElement("div");
      movieEl.classList.add("card");
      movieEl.innerHTML = `
      <h2>${name}</h2>
      <strong>Episode #${episodeId}</strong>
          <div class="card-content">            
            <p class="overview">${openingCrawl}</p>     
            <h4>Characters</h4>        
          </div>
        `;
      movieEl.appendChild(chrEl);
      main.appendChild(movieEl);
    })
  );
}

function hideloader() {
  document.getElementById("loader").style.display = "none";
}

showMovies(apiUrl);
